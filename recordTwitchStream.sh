# broadcaster="widgitybear"
broadcaster="$1"
# mailTo="Compilenix@compilenix.org"
mailTo="$2"
threads=`nproc`
exitCode=""
lastExitCode=""
set -o pipefail

while true; do
	date=$(date +%0Y-%0m-%0d.%0k%0M)
	file="twitch.tv_${broadcaster}_$date"
	logfile="${file}_livestreamer.log"
	cmd="nice -n -1 livestreamer \"twitch.tv/$broadcaster\" best -o \"$PWD/$file.flv\" -f --retry-open 10 --hds-segment-threads $(($threads / 2)) --hls-segment-threads $(($threads / 2)) --no-version-check --yes-run-as-root 1>$logfile 3>&2 | tee >(cat - >&2)"
	eval $cmd
	exitCode=$?
	if [[ $exitCode -eq 0 ]]; then
		echo
		echo -e "Subject: $broadcaster's stream has finished ($file.flv)\n$PWD/$file.flv" | sendmail "$mailTo"
		/usr/sh/ffmpegConvertTwitchStreamToMP4.sh "$PWD/$file" "flv" "$mailTo" "$broadcaster" "fatal" &
	elif [[ $exitCode -ne 1 ]]; then
		if [[ $exitCode -ne $lastExitCode ]]; then
			echo
			lastExitCode=$exitCode
			echo -e "Subject: $broadcaster's stream has encountered an error\nExit code: $exitCode\nFile: $PWD/$file.flv\nExec: $cmd\n\n`cat $logfile`" | sendmail "$mailTo"
		fi
	fi
	rm -f "$logfile"
	echo "$date -> $broadcaster"
	sleep 60
done
