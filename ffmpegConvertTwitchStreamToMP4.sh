# file="/media/data/video/stream/xFactorGaming/twitch.tv_rivalxfactor_2015-04-04.0410"
file="$1"
# fileext="flv"
fileext="$2"
# mailTo="Compilenix@compilenix.org"
mailTo="$3"
# broadcaster="widgitybear"
broadcaster="$4"
# loglevel="info"
loglevel="$5"
threads=$((`nproc`+`nproc`/2))
exitCode=""
logfile="${file}_ffmpeg.log"
cmd="nice -n 19 ffmpeg -loglevel $loglevel -hwaccel auto -i \"$file.$fileext\" -threads \"$threads\" -vcodec h264 -acodec libfaac -crf 18 -y \"$file.mp4\" > >(tee \"${logfile}\") 2>&1"
set -o pipefail

# On windows use:
# ffmpeg -hwaccel auto -i "$file.$fileext" -threads "$threads" -vcodec h264 -acodec libvo_aacenc -quality best -y "$file.mp4"

eval $cmd
exitCode=$?
# exitCode=1

if [[ $exitCode -eq 0 ]]; then
	echo -e "Subject: $broadcaster's stream converted to MP4 ($(basename $file).mp4)\n$file.mp4\n\nExec: $cmd\n\n`cat "${logfile}"`" | sendmail "$mailTo"
else
	echo -e "Subject: $broadcaster's stream converting to MP4 has encountered an error ($(basename $file).mp4)\nExit code: $exitCode\nFile: $file.mp4\nExec: $cmd\n\n`cat "${logfile}"`" | sendmail "$mailTo"
fi
rm -f "${logfile}"
